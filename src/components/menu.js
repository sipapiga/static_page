import React from "react"
import { Link } from "gatsby"

export default function menu() {
  return (
    <div style={{ backdropFilter: "#f4f4f4", paddingTop: "10px" }}>
      <ul
        style={{
          listStyle: "none",
          display: "flex",
          justifyContent: "space-evenly",
          textDecoration: "none",
        }}
      >
        <li>
          <Link to="/static_page/">Home</Link>
        </li>
        <li>
          <Link to="/static_page/description">Description</Link>
        </li>
        <li>
          <Link to="https://gitlab.com/sipapiga/static_page" target="_blank">
            Gitlab
          </Link>
        </li>
      </ul>
    </div>
  )
}
