import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"

export default function Description() {
  return (
    <Layout>
      <h2>Instructions</h2>
      <h3>Exercise CI for GitLab Pages</h3>
      <p>Create a static webpage with GitLab CI Templates</p>
      <Link
        to="https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html"
        target="_blank"
      >
        Gitlab CI gettings started
      </Link>{" "}
      <br />
      <div style={{ margin: "30px 0 0 30px" }}>
        <ul>
          <li>Create a new project</li>
          <li>
            Follow the guide for a static page, i.e hugo, jekyll, gatsby or any
            other. Use a solution based on markdown or a ready example.
          </li>
          <li>Complete the pipeline so it deploys to pages</li>
          <li>Hand in a screenshot and the .gitlab-ci.yml file</li>
        </ul>
      </div>
    </Layout>
  )
}
