import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Page two" />
    <h1>Hi from the second page</h1>
    <p>Welcome to page 2</p>
    <Link to="/">Go back to the homepage</Link>
    <ul>
      <li> Create a new project</li>
      <li>
        Follow the guide for a static page, i.e hugo, jekyll, gatsby or any
        other. Use a solution based on markdown or a ready example.
      </li>
      <li>Complete the pipeline so it deploys to pages</li>
      <li>Hand in a screenshot and the .gitlab-ci.yml file.</li>
    </ul>
  </Layout>
)

export default SecondPage
